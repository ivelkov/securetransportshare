package ivelkov.fileshare;

import ivelkov.fileshare.model.FileSystem;
import ivelkov.fileshare.model.FileSystem.TYPE;
import ivelkov.fileshare.model.FileSystemBuilder;

import java.io.IOException;

import android.content.Context;
import android.os.AsyncTask;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.plus.Plus;

public class WipeoutTask extends AsyncTask<GoogleApiClient, Void, Void> {

    private final Context mContext;

    public WipeoutTask(Context context) {
        super();
        mContext = context;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected Void doInBackground(GoogleApiClient... googleApiClients) {
        FileSystem filesystem;
        try {
            filesystem = FileSystemBuilder.build(TYPE.S3);
            filesystem.init();
            filesystem.getCurrentDirectory().delete(filesystem.getCurrentDirectory().getCurrentPathFile());
            final GoogleApiClient googleAPIClient = googleApiClients[0];
            Plus.AccountApi.clearDefaultAccount(googleAPIClient);
            googleAPIClient.disconnect();
            CognitoSyncClientManager.getInstance().wipeData();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void result) {
        super.onPostExecute(result);
    }

}
