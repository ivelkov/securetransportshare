package ivelkov.fileshare;

import ivelkov.fileshare.model.FileSystem.TYPE;
import ivelkov.fileshare.presentation.StorageFragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTabHost;
import android.view.Menu;
import android.view.MenuItem;

public class MainActivity extends FragmentActivity {

    private FragmentTabHost mTabHost;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mTabHost = (FragmentTabHost) findViewById(android.R.id.tabhost);
        mTabHost.setup(this, getSupportFragmentManager(), R.id.realtabcontent);

        Bundle local = new Bundle();
        local.putSerializable(TYPE.class.getName(), TYPE.LOCAL);
        mTabHost.addTab(mTabHost.newTabSpec("local").setIndicator("Local"), StorageFragment.class, local);
        
        Bundle cloud = new Bundle();
        cloud.putSerializable(TYPE.class.getName(), TYPE.S3);
        mTabHost.addTab(mTabHost.newTabSpec("cloud").setIndicator("Cloud"), StorageFragment.class, cloud);

    }

    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main, menu);
        getMenuInflater().inflate(R.menu.action_menu, menu);
        return true;
    }

    @Override
    public boolean onMenuItemSelected(int featureId, MenuItem item) {
        boolean handled = false;
        switch (item.getItemId()) {
            case R.id.menu_logout:
                Intent returnIntent = new Intent();
                setResult(LoginScreen.RC_SIGN_OUT,returnIntent);
                finish();
                handled = true;
                break;
        }
        return handled ? handled : super.onMenuItemSelected(featureId, item);
    }

}
