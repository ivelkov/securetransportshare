package ivelkov.fileshare.model;

import ivelkov.fileshare.CognitoSyncClientManager;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import android.util.Log;

import com.amazonaws.AmazonClientException;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CopyObjectRequest;
import com.amazonaws.services.s3.model.CopyObjectResult;
import com.amazonaws.services.s3.model.DeleteObjectRequest;
import com.amazonaws.services.s3.model.ListObjectsRequest;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3ObjectSummary;

public class S3Directory implements Directory {

    private static final long serialVersionUID = -7513754914961554544L;

    private transient AmazonS3Client mClient;

    private MyFile[] mFiles;

    private String mAccountName;

    private MyFile mDirectory;

    private final URI mAccountRootFolder;

    private final URI mCurrentFolder;

    private boolean mInitialized = false;

    public S3Directory(MyFile directory) throws IOException {
        if (directory == null || directory.isFile()) {
            directory = new MyFile(true, S3Util.DELIMETER, S3Util.DELIMETER, 0);
        }

        mDirectory = new MyFile(directory);

        mAccountName = CognitoSyncClientManager.getAccountName();
        try {
            mAccountRootFolder = S3Util.buildRemoteAccountRoot();
            mCurrentFolder = S3Util.normalize(mAccountRootFolder.resolve("./" + mDirectory.getAbsoluteFilename() + S3Util.DELIMETER));
        } catch (URISyntaxException e) {
            throw new IOException("Could not get S3 root folder", e);
        }
    }

    public S3Directory() throws IOException {
        this(null);
    }

    @Override
    public synchronized int getCount() {
        assertInitialized();
        return mFiles.length;
    }

    @Override
    public synchronized String getCurrentPath() {
        assertInitialized();
        return mDirectory.getAbsoluteFilename();
    }

    @Override
    public synchronized MyFile getCurrentPathFile() {
        assertInitialized();
        return mDirectory;
    }

    @Override
    public synchronized MyFile getFile(int position) {
        assertInitialized();
        return mFiles[position];
    }

    @Override
    public synchronized MyFile[] getFiles() {
        assertInitialized();
        return mFiles;
    }

    @Override
    public synchronized MyFile getFileByName(String name) {
        assertInitialized();
        for (MyFile file : mFiles) {
            if (file.getName().equals(name)) {
                return file;
            }
        }
        return null;
    }

    @Override
    public synchronized MyFile getParent() {
        assertInitialized();
        MyFile parent = mDirectory;
        URI newCurrentFolder;
        try {
            newCurrentFolder = S3Util.normalize(mCurrentFolder.resolve("../"));
            if (newCurrentFolder.getPath().startsWith(mAccountRootFolder.getPath())) {
                parent = S3Util.buildFile(mAccountRootFolder, newCurrentFolder.getPath(), 0);
            }
        } catch (URISyntaxException e) {
            Log.v(S3Directory.class.getName(), "Could not get parent of " + mDirectory);
        }
        return parent;
    }

    @Override
    public synchronized void init() throws IOException {
        mClient = new AmazonS3Client(CognitoSyncClientManager.getCachingCredentialsProvider());
        mClient.setRegion(Region.getRegion(Regions.EU_CENTRAL_1));
        mInitialized = true;
        refresh();

    }

    @Override
    public synchronized void refresh() throws IOException {
        assertInitialized();
        mFiles = listFilesInternal();
    }

    private MyFile[] listFilesInternal() throws IOException {
        Log.v(S3Directory.class.getName(), "Listing cloud files for account: " + mAccountName);
        ListObjectsRequest request = new ListObjectsRequest();
        final String currentPath = mCurrentFolder.getPath().substring(1);
        request.withBucketName(mCurrentFolder.getAuthority()).withPrefix(currentPath).withDelimiter(S3Util.DELIMETER);
        final ObjectListing listObjects = mClient.listObjects(request);
        final List<S3ObjectSummary> objectSummaries = listObjects.getObjectSummaries();

        final List<MyFile> filesList = new ArrayList<MyFile>(objectSummaries.size());

        for (String key : listObjects.getCommonPrefixes()) {
            if (!key.equals(currentPath)) {
                final MyFile file = S3Util.buildFile(mAccountRootFolder, key, 0);
                if (file != null) {
                    filesList.add(file);
                }
            }
        }

        for (S3ObjectSummary summary : objectSummaries) {
            final String key = summary.getKey();
            if (!key.equals(currentPath)) {
                final MyFile file = S3Util.buildFile(mAccountRootFolder, key, summary.getSize());
                if (file != null) {
                    filesList.add(file);
                }
            }
        }

        return filesList.toArray(new MyFile[filesList.size()]);
    }

    private void assertInitialized() {
        if (!mInitialized) {
            throw new IllegalStateException("Directory not initialized");
        }
    }

    @Override
    public boolean delete(MyFile myFile) {

        boolean hasData = false;
        String marker = null;
        boolean deleted = false;
        try {
            final URI uri = S3Util.buildRemoteURI(myFile);
            do {
                hasData = false;
                final ListObjectsRequest request = new ListObjectsRequest();
                final String currentPath = uri.getPath().substring(1);
                request.withBucketName(mCurrentFolder.getAuthority()).withPrefix(currentPath).withMarker(marker).setMaxKeys(50);
                final ObjectListing objects = mClient.listObjects(request);
                final List<String> commonPrefixes = objects.getCommonPrefixes();
                for (String key : commonPrefixes) {
                    mClient.deleteObject(new DeleteObjectRequest(uri.getAuthority(), key));
                    hasData = true;
                }
                final List<S3ObjectSummary> objectSummaries = objects.getObjectSummaries();
                for (S3ObjectSummary s3ObjectSummary : objectSummaries) {
                    final String key = s3ObjectSummary.getKey();
                    mClient.deleteObject(new DeleteObjectRequest(uri.getAuthority(), key));
                    hasData = true;
                }
                marker = objects.getNextMarker();
            } while (hasData);

            deleted = true;
        } catch (URISyntaxException e) {
            Log.v(S3Directory.class.getName(), e.getMessage());
        } catch (AmazonClientException e) {
            Log.v(S3Directory.class.getName(), e.getMessage());
        }
        return deleted;
    }

    @Override
    public boolean newDirectory(String folder) {
        boolean result = false;
        // create meta-data for your folder and set content-length to 0
        ObjectMetadata metadata = new ObjectMetadata();
        metadata.setContentLength(0);

        // create empty content
        InputStream emptyContent = new ByteArrayInputStream(new byte[0]);

        // create a PutObjectRequest passing the folder name suffixed by /
        PutObjectRequest putObjectRequest = new PutObjectRequest(mCurrentFolder.getAuthority(), mCurrentFolder.getPath().substring(1) + folder + S3Util.DELIMETER, emptyContent,
                metadata);

        // send request to S3 to create folder
        try {
            mClient.putObject(putObjectRequest);
            result = true;
        } catch (AmazonClientException e) {
            Log.e(S3Directory.class.getName(), "Could not create new directory", e);
        }
        return result;
    }

    @Override
    public boolean rename(MyFile file, String newName) {
        URI source;
        try {
            source = S3Util.buildRemoteURI(file);
            final String sourceKey = source.getPath().substring(1);
            final String targetKey = mCurrentFolder.getPath().substring(1) + newName;
            final String bucket = source.getAuthority();
            CopyObjectRequest request = new CopyObjectRequest(bucket, sourceKey, bucket, targetKey);
            final CopyObjectResult result = mClient.copyObject(request);
            return delete(file);
        } catch (URISyntaxException e) {
            Log.e(S3Directory.class.getName(), "Could not build URI for source file " + file, e);
        } catch (AmazonClientException e) {
            Log.e(S3Directory.class.getName(), "Could not rename file "+ file, e);
        }
        return false;
    }
}
