package ivelkov.fileshare.model;

import ivelkov.fileshare.model.FileSystem.Memento;
import ivelkov.fileshare.model.FileSystem.TYPE;

public class FileSystemMemento implements Memento {

    private static final long serialVersionUID = -7652671336495277368L;
    
    private final Directory mDirectory;

    private final TYPE mType;

    public FileSystemMemento(Directory directory, TYPE type) {
        mDirectory = directory;
        mType = type;
    }

    @Override
    public Directory getDirectory() {
        return mDirectory;
    }
    
    @Override
    public TYPE getType() {
        return mType;
    }

}