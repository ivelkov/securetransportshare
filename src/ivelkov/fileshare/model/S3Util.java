package ivelkov.fileshare.model;

import ivelkov.fileshare.CognitoSyncClientManager;

import java.net.URI;
import java.net.URISyntaxException;

import android.util.Log;

public class S3Util {

    private static final String SCHEME = "s3";

    public static final String DELIMETER = "/";

    public static MyFile buildFile(URI accountRoot, String s3key, long fileSize) {
        MyFile file = null;
        try {
            final URI key = normalize(new URI(SCHEME, CognitoSyncClientManager.getBucketName(), DELIMETER + s3key, null, null));
            if (key.getPath().startsWith(accountRoot.getPath())) {
                String path = key.getPath().substring(accountRoot.getPath().length() - 1);
                boolean isFolder = false;
                if (path.endsWith(DELIMETER)) {
                    isFolder = true;
                }
                if (path.length() > 1) {
                    path = path.substring(0, path.length() - 1);
                }
                String name = path.contains(DELIMETER) ? path.substring(path.lastIndexOf(DELIMETER) + 1) : path;
                file = new MyFile(isFolder, path, name.length() == 0 ? "/" : name, fileSize);
            }
        } catch (URISyntaxException e) {
            Log.e(S3Util.class.getName(), "Could not build URI for " + s3key);
        }

        return file;
    }

    public static URI buildRemoteURI(MyFile file) throws URISyntaxException {
        return normalize(new URI(SCHEME, CognitoSyncClientManager.getBucketName(), "/cognito/cloudshare/" + CognitoSyncClientManager.getAccountName() + file.getAbsoluteFilename()
                + (file.isDirectory() ? DELIMETER : ""), null, null));
    }

    public static URI buildRemoteAccountRoot() throws URISyntaxException {
        return normalize(new URI(SCHEME, CognitoSyncClientManager.getBucketName(), "/cognito/cloudshare/" + CognitoSyncClientManager.getAccountName() + DELIMETER, null, null));
    }

    public static URI normalize(URI uri) throws URISyntaxException {
        uri = uri.normalize();
        uri = new URI(uri.getScheme(), uri.getAuthority(), uri.getPath().replaceAll("/{2,}", "/"), uri.getQuery(), uri.getFragment());
        return uri;
    }

}
