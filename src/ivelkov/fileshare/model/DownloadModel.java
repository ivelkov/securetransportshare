/*
 * Copyright 2010-2014 Amazon.com, Inc. or its affiliates. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License.
 * A copy of the License is located at
 *
 *  http://aws.amazon.com/apache2.0
 *
 * or in the "license" file accompanying this file. This file is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

package ivelkov.fileshare.model;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;

import com.amazonaws.event.ProgressEvent;
import com.amazonaws.event.ProgressListener;
import com.amazonaws.mobileconnectors.s3.transfermanager.Download;
import com.amazonaws.mobileconnectors.s3.transfermanager.PersistableDownload;
import com.amazonaws.mobileconnectors.s3.transfermanager.Transfer;
import com.amazonaws.mobileconnectors.s3.transfermanager.TransferManager;
import com.amazonaws.mobileconnectors.s3.transfermanager.exception.PauseException;

/*
 * Class that encapsulates downloads, handling all the interaction with the
 * underlying Download and TransferManager
 */
public class DownloadModel extends TransferModel {
    private static final String TAG = "DownloadModel";

    private Download mDownload;
    private PersistableDownload mPersistableDownload;
    private ProgressListener mListener;
    private Status mStatus;

    public DownloadModel(Context context, MyFile localFile, MyFile remoteFile, TransferManager manager) {
        super(context, localFile, remoteFile, manager);
        mStatus = Status.IN_PROGRESS;
        mListener = new ProgressListener() {
            @Override
            public void progressChanged(ProgressEvent event) {
                if (event.getEventCode() == ProgressEvent.COMPLETED_EVENT_CODE) {

                    Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                    mediaScanIntent.setData(Uri.fromFile(new File(getLocalFile().getAbsoluteFilename())));
                    getContext().sendBroadcast(mediaScanIntent);

                    mStatus = Status.COMPLETED;

                }
            }
        };
    }

    @Override
    public Status getStatus() {
        return mStatus;
    }

    @Override
    public Transfer getTransfer() {
        return mDownload;
    }

    @Override
    public void abort() {
        if (mDownload != null) {
            mStatus = Status.CANCELED;
            try {
                mDownload.abort();
            } catch (IOException e) {
                Log.e(TAG, "", e);
            }
        }
    }

    public void download() {
        mStatus = Status.IN_PROGRESS;
        final File file = new File(getLocalFile().getAbsoluteFilename());
        URI remoteURI;
        try {
            remoteURI = S3Util.buildRemoteURI(getRemoteFile());
            mDownload = getTransferManager().download(remoteURI.getAuthority(), remoteURI.getPath().substring(1), file);
            if (mListener != null) {
                mDownload.addProgressListener(mListener);
            }
        } catch (URISyntaxException e) {
            Log.d(TAG, "", e);
        }

    }

    @Override
    public void pause() {
        if (mStatus == Status.IN_PROGRESS) {
            mStatus = Status.PAUSED;
            try {
                mPersistableDownload = mDownload.pause();
            } catch (PauseException e) {
                Log.d(TAG, "", e);
            }
        }
    }

    @Override
    public void resume() {
        if (mStatus == Status.PAUSED) {
            mStatus = Status.IN_PROGRESS;
            if (mPersistableDownload != null) {
                mDownload = getTransferManager().resumeDownload(mPersistableDownload);
                mDownload.addProgressListener(mListener);
                mPersistableDownload = null;
            } else {
                download();
            }
        }
    }
}
