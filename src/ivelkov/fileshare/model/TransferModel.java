package ivelkov.fileshare.model;

import java.util.LinkedHashMap;

import android.content.Context;

import com.amazonaws.mobileconnectors.s3.transfermanager.Transfer;
import com.amazonaws.mobileconnectors.s3.transfermanager.TransferManager;

/*
 * The TransferModel is a class that encapsulates a transfer. It handles the 
 * interaction with the underlying TransferManager and Upload/Download classes
 */
public abstract class TransferModel {

    public static enum Status {
        IN_PROGRESS, PAUSED, CANCELED, COMPLETED, FAILED
    };

    // all TransferModels have associated id which is their key to sModels
    private static LinkedHashMap<Integer, TransferModel> sModels = new LinkedHashMap<Integer, TransferModel>();
    private static int sNextId = 1;

    private Context mContext;
    private int mId;
    private TransferManager mManager;
    private final MyFile mLocalFile;
    private final MyFile mRemoteFile;

    public static TransferModel getTransferModel(int id) {
        return sModels.get(id);
    }

    public static TransferModel[] getAllTransfers() {
        TransferModel[] models = new TransferModel[sModels.size()];
        return sModels.values().toArray(models);
    }

    public TransferModel(Context context, MyFile localFile, MyFile remoteFile, TransferManager manager) {
        mContext = context;
        mLocalFile = localFile;
        mRemoteFile = remoteFile;
        mManager = manager;
        mId = sNextId++;
        sModels.put(mId, this);
    }

    public int getId() {
        return mId;
    }

    public int getProgress() {
        Transfer transfer = getTransfer();
        if (transfer != null) {
            int ret = (int) transfer.getProgress().getPercentTransferred();
            return ret;
        }
        return 0;
    }

    public abstract void abort();

    public abstract Status getStatus();

    public abstract Transfer getTransfer();

    public abstract void pause();

    public abstract void resume();

    protected Context getContext() {
        return mContext;
    }

    protected TransferManager getTransferManager() {
        return mManager;
    }
    
    protected MyFile getLocalFile () {
        return mLocalFile;
    }
    
    protected MyFile getRemoteFile () {
        return mRemoteFile;
    }
}
