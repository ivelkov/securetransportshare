package ivelkov.fileshare.model;

import java.io.IOException;
import java.io.Serializable;

public interface Directory extends Serializable {

    int getCount();

    String getCurrentPath();

    MyFile getCurrentPathFile();

    MyFile getFile(int position);

    MyFile[] getFiles();

    MyFile getFileByName(String name);

    MyFile getParent();

    /**
     * Initialize the directory.
     * <p>
     * Call it before calling any of the getters. This prefetches information
     * from the filesystem. Could be a time consuming operation.
     * 
     * @throws IOException .
     */
    void init() throws IOException;

    /**
     * Refreshes the directory.
     * <p>
     * Could be a time consuming operation.
     * 
     * @throws IOException .
     */
    void refresh() throws IOException;

    boolean delete(MyFile myFile);

    boolean newDirectory(String string);

    boolean rename(MyFile file, String newName);

}
