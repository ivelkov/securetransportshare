package ivelkov.fileshare.model;

import java.io.IOException;

import ivelkov.fileshare.model.FileSystem.Memento;
import ivelkov.fileshare.model.FileSystem.TYPE;

public class FileSystemBuilder {
    
    public static FileSystem build(TYPE type) throws IOException {
        return build(type, null);
    }
    
    public static FileSystem build(TYPE type, MyFile file) throws IOException {
        switch (type) {
            case LOCAL:
                if (file != null) {
                    return new LocalFileSystem(file);
                } else {
                    return new LocalFileSystem();
                }
            case S3:
                return new S3FileSystem();
            default:
                throw new UnsupportedOperationException("Type " + type + " is not supported.");
                
        }
    }
    
    public static FileSystem restore(Memento memento) throws IOException {
        final TYPE type = memento.getType();
        switch (type) {
            case LOCAL:
                final FileSystem fs = build(type);
                fs.restore(memento);
                return fs;
            case S3:
                return new S3FileSystem();
            default:
                throw new UnsupportedOperationException("Type " + type + " is not supported.");
            
        }
    }

}
