package ivelkov.fileshare.model;

import java.io.IOException;


public class LocalFileSystem implements FileSystem {

    private static final long serialVersionUID = 6231228744658063347L;
    
    private Directory mCurrentDirectory;

    LocalFileSystem() {
        mCurrentDirectory = new LocalDirectory();
    }

    LocalFileSystem(MyFile directory) {
        mCurrentDirectory = new LocalDirectory(directory);
    }
    
    public void init() throws IOException {
        mCurrentDirectory.init();
    }

    @Override
    public Directory getCurrentDirectory() {
        return mCurrentDirectory;
    }

    @Override
    public void changeDirectory(MyFile file) throws IOException {
        mCurrentDirectory = new LocalDirectory(file);
        mCurrentDirectory.init();

    }

    @Override
    public Memento save() {
        return new FileSystemMemento(getCurrentDirectory(), getType());
    }

    @Override
    public void restore(Memento memento) {
        mCurrentDirectory = memento.getDirectory();
    }

    @Override
    public TYPE getType() {
        return TYPE.LOCAL;
    }

}
