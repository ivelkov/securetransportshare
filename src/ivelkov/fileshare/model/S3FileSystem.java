package ivelkov.fileshare.model;

import java.io.IOException;

public class S3FileSystem implements FileSystem {

    private static final long serialVersionUID = 6231228744658063347L;

    private Directory mCurrentDirectory;

    public S3FileSystem() throws IOException {
        mCurrentDirectory = new S3Directory();
    }

    public S3FileSystem(MyFile directory) throws IOException {
        mCurrentDirectory = new S3Directory(directory);
    }

    @Override
    public Directory getCurrentDirectory() {
        return mCurrentDirectory;
    }

    @Override
    public void changeDirectory(MyFile file) throws IOException {
        mCurrentDirectory = new S3Directory(file);
        mCurrentDirectory.init();
    }

    @Override
    public Memento save() {
        return new FileSystemMemento(getCurrentDirectory(), getType());
    }

    @Override
    public void restore(Memento memento) {
        mCurrentDirectory = memento.getDirectory();
    }

    @Override
    public TYPE getType() {
        return TYPE.S3;
    }

    @Override
    public void init() throws IOException {
        mCurrentDirectory.init(); 
    }
    
    void wipeout() {
       mCurrentDirectory.delete(mCurrentDirectory.getCurrentPathFile()); 
    }

}
