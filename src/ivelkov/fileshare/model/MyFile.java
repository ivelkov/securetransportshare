package ivelkov.fileshare.model;

import java.io.Serializable;

public class MyFile implements Serializable {

    private static final long serialVersionUID = 2060513682790139004L;

    private final boolean isDirectory;
    
    private final String mAbsoluteFilename;
    
    private String mName;

    private final long mSize;
    
    public MyFile(boolean isDirectory, String absoluteFilename, String name, long size) {
        super();
        this.isDirectory = isDirectory;
        mAbsoluteFilename = absoluteFilename;
        mName = name;
        mSize = size;
    }
    
    public MyFile(MyFile file) {
        this(file.isDirectory, file.getAbsoluteFilename(), file.getName(), file.length());
    }

    public boolean isDirectory() {
        return isDirectory;
    }
    
    public boolean isFile() {
        return !isDirectory;
    }

    public String getAbsoluteFilename() {
        return mAbsoluteFilename;
    }
    
    public String getName() {
        return mName;
    }

    public long length() {
        return mSize;
    }
    
    @Override
    public String toString() {
        return "MyFile [mName=" + mName + ", mAbsoluteFilename=" + mAbsoluteFilename + ", isDirectory=" + isDirectory + ", mSize=" + mSize + "]";
    }

}
