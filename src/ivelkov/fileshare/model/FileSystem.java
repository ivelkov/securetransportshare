package ivelkov.fileshare.model;

import java.io.IOException;
import java.io.Serializable;

public interface FileSystem extends Serializable {

    enum TYPE {
        LOCAL, S3
    }

    /**
     * Initialize the filesystem.
     * <p>
     * Call it before oeprating with it. Could be a time-consuming operation.
     * 
     * @throws IOException .
     */
    void init() throws IOException;

    Directory getCurrentDirectory();

    void changeDirectory(MyFile file) throws IOException;

    Memento save();

    void restore(Memento memento);

    TYPE getType();

    interface Memento extends Serializable {
        Directory getDirectory();

        TYPE getType();
    }

}
