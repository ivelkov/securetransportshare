package ivelkov.fileshare.model;

import java.io.File;
import java.io.IOException;

import android.os.Environment;

public class LocalDirectory implements Directory {

    private static final long serialVersionUID = 8880737025765296649L;

    private final MyFile mDirectory;
    private MyFile[] mFiles;

    private final File mNativeDirectory;

    private boolean mInitialized = false;

    public LocalDirectory(MyFile directory) {
        mDirectory = directory;
        mNativeDirectory = new File(mDirectory.getAbsoluteFilename());
    }

    public LocalDirectory() {
        this(new MyFile(true, Environment.getRootDirectory().getAbsolutePath(), Environment.getRootDirectory().getName(), 0));
    }

    @Override
    public synchronized int getCount() {
        assertInitialized();
        return mFiles.length;
    }

    @Override
    public synchronized String getCurrentPath() {
        assertInitialized();
        return mDirectory.getAbsoluteFilename();
    }

    @Override
    public synchronized MyFile getFile(int position) {
        assertInitialized();
        return mFiles[position];
    }

    @Override
    public synchronized MyFile[] getFiles() {
        assertInitialized();
        return mFiles;
    }

    @Override
    public synchronized MyFile getCurrentPathFile() {
        assertInitialized();
        return mDirectory;
    }

    @Override
    public synchronized MyFile getFileByName(String name) {
        assertInitialized();
        for (MyFile file : mFiles) {
            if (file.getName().equals(name)) {
                return file;
            }
        }
        return null;
    }

    @Override
    public synchronized MyFile getParent() {
        assertInitialized();
        final File parentNativeFile = mNativeDirectory.getParentFile();
        if (parentNativeFile != null) {
            return new MyFile(true, parentNativeFile.getAbsolutePath(), parentNativeFile.getName(), 0);
        }
        return mDirectory;
    }

    @Override
    public synchronized void init() throws IOException {
        mInitialized = true;
        refresh();
    }

    @Override
    public synchronized void refresh() throws IOException {
        assertInitialized();
        final File[] localFiles = mNativeDirectory.listFiles();
        mFiles = new MyFile[localFiles == null ? 0 : localFiles.length];
        for (int i = 0; i < mFiles.length; i++) {
            final File localFile = localFiles[i];
            mFiles[i] = new MyFile(localFile.isDirectory(), localFile.getAbsolutePath(), localFile.getName(), localFile.length());
        }
    }

    private void assertInitialized() {
        if (!mInitialized) {
            throw new IllegalStateException("Directory not initialized");
        }
    }

    @Override
    public boolean delete(MyFile myFile) {
        final File nativeFile = new File(myFile.getAbsoluteFilename());
        return deleteInternal(nativeFile);
    }

    private boolean deleteInternal(File file) {
        boolean result = false;
        if (file.isFile()) {
            result = file.delete();
        } else if (file.isDirectory()) {
            for (File f : file.listFiles()) {
                if (f.isFile()) {
                    result &= f.delete();
                } else {
                    result &= deleteInternal(f);
                }
            }
        } // else special file
        return result;
    }

    @Override
    public boolean newDirectory(String string) {
        File file = new File(mNativeDirectory, string);
        return file.mkdir();
    }

    @Override
    public boolean rename(MyFile file, String newName) {
        return new File(file.getAbsoluteFilename()).renameTo(new File(mNativeDirectory, newName));
    }

}
