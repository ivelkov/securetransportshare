package ivelkov.fileshare.model;

import ivelkov.fileshare.MainActivity;
import ivelkov.fileshare.R;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.RemoteViews;

import com.amazonaws.event.ProgressEvent;
import com.amazonaws.event.ProgressListener;
import com.amazonaws.mobileconnectors.s3.transfermanager.PersistableUpload;
import com.amazonaws.mobileconnectors.s3.transfermanager.Transfer;
import com.amazonaws.mobileconnectors.s3.transfermanager.TransferManager;
import com.amazonaws.mobileconnectors.s3.transfermanager.Upload;
import com.amazonaws.mobileconnectors.s3.transfermanager.exception.PauseException;

/* UploadModel handles the interaction between the Upload and TransferManager.
 * This also makes sure that the file that is uploaded has the same file extension
 *
 * One thing to note is that we always create a copy of the file we are given. This
 * is because we wanted to demonstrate pause/resume which is only possible with a
 * File parameter, but there is no reliable way to get a File from a Uri(mainly
 * because there is no guarantee that the Uri has an associated File).
 *
 * You can easily avoid this by directly using an InputStream instead of a Uri.
 */
public class UploadModel extends TransferModel {
    private static final String TAG = "UploadModel";

    private Upload mUpload;
    private PersistableUpload mPersistableUpload;
    private ProgressListener mListener;
    private Status mStatus;
    private File mFile;

    private Notification notification = null;

    public UploadModel(Context context, MyFile localFile, MyFile remoteFile, TransferManager manager) {
        super(context, localFile, remoteFile, manager);
        mStatus = Status.IN_PROGRESS;
        mListener = new ProgressListener() {
            @Override
            public void progressChanged(ProgressEvent event) {
                final Context applicationContext = getContext().getApplicationContext();
                if (event.getEventCode() == ProgressEvent.STARTED_EVENT_CODE) {
                    if (notification != null) {
                        notification.contentView.setProgressBar(R.id.status_progress, 100, 0, false);
                        final NotificationManager notificationManager = (NotificationManager) applicationContext.getSystemService(Context.NOTIFICATION_SERVICE);
                        notificationManager.notify(getId(), notification);
                    }
                }

                if (event.getEventCode() == ProgressEvent.PART_COMPLETED_EVENT_CODE) {
                    final int progress = (int) (((double) event.getBytesTransferred() / getLocalFile().length()) * 100);
                    notification.contentView.setProgressBar(R.id.status_progress, 100, progress, false);
                    final NotificationManager notificationManager = (NotificationManager) applicationContext.getSystemService(Context.NOTIFICATION_SERVICE);
                    notificationManager.notify(getId(), notification);
                }

                if (event.getEventCode() == ProgressEvent.COMPLETED_EVENT_CODE) {
                    mStatus = Status.COMPLETED;
                    if (mFile != null) {
                        mFile.delete();
                    }
                    if (notification != null) {
                        notification.contentView.setProgressBar(R.id.status_progress, 100, 100, false);
                        final NotificationManager notificationManager = (NotificationManager) applicationContext.getSystemService(Context.NOTIFICATION_SERVICE);
                        notificationManager.notify(getId(), notification);
                    }
                }

                if (event.getEventCode() == ProgressEvent.FAILED_EVENT_CODE) {
                    mStatus = Status.FAILED;
                    if (mFile != null) {
                        mFile.delete();
                    }
                }
            }
        };
    }

    public Runnable getUploadRunnable() {
        return new Runnable() {
            @Override
            public void run() {
                upload();
            }
        };
    }

    @Override
    public void abort() {
        if (mUpload != null) {
            mStatus = Status.CANCELED;
            mUpload.abort();
            if (mFile != null) {
                mFile.delete();
            }
        }
    }

    @Override
    public Status getStatus() {
        return mStatus;
    }

    @Override
    public Transfer getTransfer() {
        return mUpload;
    }

    @Override
    public void pause() {
        if (mStatus == Status.IN_PROGRESS) {
            if (mUpload != null) {
                mStatus = Status.PAUSED;
                try {
                    mPersistableUpload = mUpload.pause();
                } catch (PauseException e) {
                    Log.d(TAG, "", e);
                }
            }
        }
    }

    @Override
    public void resume() {
        if (mStatus == Status.PAUSED) {
            mStatus = Status.IN_PROGRESS;
            if (mPersistableUpload != null) {
                // if it paused fine, resume
                mUpload = getTransferManager().resumeUpload(mPersistableUpload);
                mUpload.addProgressListener(mListener);
                mPersistableUpload = null;
            } else {
                // if it was actually aborted, start a new one
                upload();
            }
        }
    }

    public void upload() {
        if (mFile == null) {
            saveTempFile();
        }
        if (mFile != null) {
            try {
                notification = new Notification.Builder(getContext()).setContentText("Upload").setContentInfo("Uploading " + getLocalFile().getName())
                        .setSmallIcon(R.drawable.file_upload).setAutoCancel(true).build();

                notification.flags = notification.flags | Notification.FLAG_ONGOING_EVENT;

                final Context applicationContext = getContext().getApplicationContext();
                notification.contentView = new RemoteViews(applicationContext.getPackageName(), R.layout.transfer_progress);

                PendingIntent notifyIntent = PendingIntent
                        .getActivity(applicationContext, 0, new Intent(applicationContext, MainActivity.class), PendingIntent.FLAG_UPDATE_CURRENT);

                notification.contentIntent = notifyIntent;
                notification.contentView.setImageViewResource(R.id.status_icon, R.drawable.file_upload);

                notification.contentView.setTextViewText(R.id.status_text, "Uploading " + getLocalFile().getName());
                notification.contentView.setProgressBar(R.id.status_progress, 100, 0, false);

                final URI remoteURI = S3Util.buildRemoteURI(getRemoteFile());
                mUpload = getTransferManager().upload(remoteURI.getAuthority(), remoteURI.getPath().substring(1), mFile);
                mUpload.addProgressListener(mListener);
            } catch (Exception e) {
                Log.e(TAG, "", e);
            }
        }
    }

    private void saveTempFile() {
        InputStream in = null;
        OutputStream out = null;

        try {
            in = new FileInputStream(new File(getLocalFile().getAbsoluteFilename()));
            mFile = File.createTempFile(getContext().getApplicationInfo().name + String.valueOf(getId()), "temp", getContext().getCacheDir());
            out = new FileOutputStream(mFile, false);
            byte[] buffer = new byte[1024];
            int read;
            while ((read = in.read(buffer)) != -1) {
                out.write(buffer, 0, read);
            }
        } catch (IOException e) {
            Log.e(TAG, "", e);
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    Log.e(TAG, "", e);
                }
            }
            if (out != null) {
                try {
                    out.close();
                } catch (IOException e) {
                    Log.e(TAG, "", e);
                }
            }
        }
    }
}
