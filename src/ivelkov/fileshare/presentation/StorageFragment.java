package ivelkov.fileshare.presentation;

import ivelkov.fileshare.R;
import ivelkov.fileshare.model.FileSystem;
import ivelkov.fileshare.model.FileSystem.Memento;
import ivelkov.fileshare.model.FileSystem.TYPE;
import ivelkov.fileshare.model.FileSystemBuilder;
import ivelkov.fileshare.model.MyFile;
import ivelkov.fileshare.network.TransferController;

import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayDeque;
import java.util.LinkedList;
import java.util.Locale;
import java.util.Queue;
import java.util.logging.Level;
import java.util.logging.Logger;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.text.InputType;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.ActionMode;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.AbsListView;
import android.widget.AbsListView.MultiChoiceModeListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class StorageFragment extends Fragment {

    private static final Logger sLogger = Logger.getLogger(StorageFragment.class.getName());

    private FileSystem mFileSystem;

    private ArrayDeque<FileSystem.Memento> mUndoQueue;

    private ArrayDeque<FileSystem.Memento> mRedoQueue;

    private GridView mGridView;

    private final DecimalFormat mDecimalFormat = new DecimalFormat("###.##");

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        boolean handled = false;
        switch (item.getItemId()) {
            case R.id.menu_back:
                undo();
                handled = true;
                break;
            case R.id.menu_up:
                MyFile parentFile = mFileSystem.getCurrentDirectory().getParent();
                if (parentFile != null) {
                    changeDirectory(parentFile);
                }
                handled = true;
                break;
            case R.id.menu_forward:
                redo();
                handled = true;
                break;
            case R.id.menu_refresh:
                refresh();
                handled = true;
                break;
            case R.id.menu_folder_add:
                newFolder();
                handled = true;
                break;
        }
        return handled;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (mFileSystem == null) {
            try {
                restore(savedInstanceState != null ? savedInstanceState : getArguments());
            } catch (IOException e) {
                throw new RuntimeException("Could not restore instance", e);
            }
        }

        final View layoutView = inflater.inflate(R.layout.storage_fragment, container, false);
        mGridView = (GridView) layoutView.findViewById(R.id.localstorage_gridview);
        final OnItemClickListener onItemClickListener = new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                final String fileStr = (String) mGridView.getAdapter().getItem(position);
                MyFile file = mFileSystem.getCurrentDirectory().getFileByName(fileStr);
                if (file.isDirectory()) {
                    changeDirectory(file);
                } else {
                    openFile(file);
                }
            }

        };
        mGridView.setOnItemClickListener(onItemClickListener);
        mGridView.setChoiceMode(GridView.CHOICE_MODE_MULTIPLE_MODAL);
        mGridView.setMultiChoiceModeListener(new MultiChoiceModeListener() {

            @Override
            public void onItemCheckedStateChanged(ActionMode mode, int position, long id, boolean checked) {
                final int firstVisiblePosition = mGridView.getFirstVisiblePosition();
                final View itemView = mGridView.getChildAt(position - firstVisiblePosition);
                final ImageView image = (ImageView) itemView.findViewById(R.id.imageView1);
                final MyFile file = mFileSystem.getCurrentDirectory().getFile(position);
                if (checked) {
                    if (file.isFile()) {
                        image.setImageResource(R.drawable.file_accept);
                    } else {
                        image.setImageResource(R.drawable.folder_accept);
                    }
                } else {
                    if (file.isFile()) {
                        image.setImageResource(R.drawable.file);
                    } else {
                        image.setImageResource(R.drawable.folder_full);
                    }
                }
            }

            @Override
            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                boolean handled = false;
                final SparseBooleanArray checked = mGridView.getCheckedItemPositions();
                // Pickup folder
                final LinkedList<MyFile> files = new LinkedList<MyFile>();
                for (int i = 0; i < checked.size(); i++) {
                    final int key = checked.keyAt(i);
                    if (checked.get(key)) {

                        final MyFile file = mFileSystem.getCurrentDirectory().getFile(key);
                        switch (item.getItemId()) {
                            case R.id.menu_upload:
                                if (mFileSystem.getType() != TYPE.LOCAL) {
                                    Toast.makeText(getActivity(), "Can upload only from a local file system.", Toast.LENGTH_SHORT).show();
                                    break;
                                }
                                if (file.isDirectory()) {
                                    Toast.makeText(getActivity(), "Upload of directories is not supported.", Toast.LENGTH_SHORT).show();
                                    break;
                                }

                                if (mFileSystem.getType() == TYPE.LOCAL) {
                                    handled = true;
                                    files.add(file);
                                    // final MyFile remoteFile = new
                                    // MyFile(false, "/" + file.getName(),
                                    // file.getName(), file.length());
                                    // TransferController.upload(StorageFragment.this.getActivity(),
                                    // file, remoteFile);
                                    // Log.v(StorageFragment.class.getName(),
                                    // "Uploading " + file);
                                }
                                break;
                            case R.id.menu_download:
                                if (mFileSystem.getType() == TYPE.S3) {
                                    handled = true;
                                } else {
                                    throw new UnsupportedOperationException("Download is only supported from a local file system");
                                }
                                break;
                            case R.id.menu_delete:
                                handled = true;
                                new AlertDialog.Builder(StorageFragment.this.getActivity()).setTitle("Delete " + file.getName() + "?")
                                        .setMessage("Are you sure you want to delete " + file.getName() + "?\r\nRecovery will not be possible.")
                                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                new DeleteTask().execute(file);
                                            }

                                        }).setNegativeButton("No", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                dialog.cancel();
                                            }
                                        }).show();
                                break;
                            case R.id.menu_rename:
                                renameFile(file);
                                handled = true;

                        }

                    }

                }

                if (!files.isEmpty() && (item.getItemId() == R.id.menu_upload || item.getItemId() == R.id.menu_download)) {
                    final Dialog dialog = new Dialog(StorageFragment.this.getActivity());
                    dialog.setContentView(R.layout.transfer_activity);
                    dialog.setTitle(R.string.transfer_dialog_title);
                    dialog.setCancelable(false);
                    final TextView text = (TextView) dialog.findViewById(R.id.transfer_dialog_uploading);
                    final StringBuilder builder = new StringBuilder(text.getText());
                    for (MyFile file : files) {
                        builder.append(file.getName()).append(", ");
                    }

                    text.setText(builder.substring(0, builder.length() - 2));
                    try {
                        final FileSystem fileSystem = FileSystemBuilder.build(TYPE.S3);
                        final AbsListView listView = (AbsListView) dialog.findViewById(R.id.transfer_dialog_listview);
                        listView.setOnItemClickListener(new OnItemClickListener() {

                            @Override
                            public void onItemClick(AdapterView<?> adapter, View view, int position, long id) {
                                final String fileStr = (String) listView.getAdapter().getItem(position);
                                final MyFile file = fileSystem.getCurrentDirectory().getFileByName(fileStr);
                                if (file.isDirectory()) {
                                    new ChangeDirectoryTask(getActivity(), listView, fileSystem).execute(file);
                                }
                            }
                        });
                        new InitializeTask(StorageFragment.this.getActivity(), listView, fileSystem).execute();

                        final Button upButton = (Button) dialog.findViewById(R.id.transfer_dialog_back);
                        upButton.setOnClickListener(new OnClickListener() {

                            @Override
                            public void onClick(View v) {
                                final MyFile file = fileSystem.getCurrentDirectory().getParent();
                                if (file.isDirectory()) {
                                    new ChangeDirectoryTask(getActivity(), listView, fileSystem).execute(file);
                                }
                            }
                        });

                        final Button cancelButton = (Button) dialog.findViewById(R.id.transfer_dialog_cancel);
                        cancelButton.setOnClickListener(new OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.cancel();
                            }
                        });
                        final Button uploadButton = (Button) dialog.findViewById(R.id.transfer_dialog_upload);
                        uploadButton.setOnClickListener(new OnClickListener() {

                            @Override
                            public void onClick(View v) {
                                for (MyFile file : files) {
                                    final MyFile remoteFile = new MyFile(false, fileSystem.getCurrentDirectory().getCurrentPath() + file.getName(), file.getName(), file.length());
                                    TransferController.upload(StorageFragment.this.getActivity(), file, remoteFile);
                                }
                                dialog.dismiss();
                            }
                        });

                    } catch (IOException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }

                    dialog.show();
                }

                if (handled) {
                    mode.finish();
                }
                return handled;
            }

            @Override
            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                final MenuInflater inflater = mode.getMenuInflater();
                switch (mFileSystem.getType()) {
                    case LOCAL:
                        inflater.inflate(R.menu.action_menu_selection_local, menu);
                        break;
                    case S3:
                        inflater.inflate(R.menu.action_menu_selection_cloud, menu);
                        break;
                    default:
                        throw new UnsupportedOperationException("Not a supported file system type " + mFileSystem.getType());

                }
                return true;
            }

            @Override
            public void onDestroyActionMode(ActionMode mode) {
                // Here you can make any necessary updates to the activity
                // when
                // the CAB is removed. By default, selected items are
                // deselected/unchecked.
            }

            @Override
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                // Here you can perform updates to the CAB due to
                // an invalidate() request
                return false;
            }
        });
        mGridView.setGravity(Gravity.TOP);
        new InitializeTask(StorageFragment.this.getActivity(), mGridView, mFileSystem).execute();
        setHasOptionsMenu(true);
        return layoutView;
    }

    @SuppressWarnings("unchecked")
    private void restore(Bundle arguments) throws IOException {
        if (arguments != null && arguments.containsKey("savedMemento")) {
            final Memento memento = (Memento) arguments.get("savedMemento");
            mFileSystem = FileSystemBuilder.restore(memento);
        } else {
            TYPE type = TYPE.LOCAL;
            if (arguments != null && arguments.containsKey(TYPE.class.getName())) {
                type = (TYPE) arguments.get(TYPE.class.getName());
            }
            mFileSystem = FileSystemBuilder.build(type);
        }

        if (arguments != null && arguments.containsKey("savedUndoMementos")) {
            mUndoQueue = (ArrayDeque<Memento>) arguments.get("savedUndoMementos");
        } else {
            mUndoQueue = new ArrayDeque<FileSystem.Memento>(10);
        }

        if (arguments != null && arguments.containsKey("savedRedoMementos")) {
            mRedoQueue = (ArrayDeque<Memento>) arguments.get("savedRedoMementos");
        } else {
            mRedoQueue = new ArrayDeque<FileSystem.Memento>(10);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle args) {
        super.onSaveInstanceState(args);
        args.putSerializable("savedMemento", mFileSystem.save());
        args.putSerializable("savedUndoMementos", mUndoQueue);
        args.putSerializable("savedRedoMementos", mRedoQueue);
    }

    private void undo() {
        changeState(mUndoQueue, mRedoQueue);
    }

    private void redo() {
        changeState(mRedoQueue, mUndoQueue);
    }

    private void refresh() {
        new RefreshTask().execute();
    }

    private void changeDirectory(MyFile file) {
        new ChangeDirectoryTask(getActivity(), mGridView, mFileSystem).execute(file);
    }

    private void openFile(MyFile file) {
        File nativeFile = new File(file.getAbsoluteFilename());
        MimeTypeMap map = MimeTypeMap.getSingleton();
        String ext = MimeTypeMap.getFileExtensionFromUrl(file.getName());
        String type = map.getMimeTypeFromExtension(ext);

        if (type == null)
            type = "*/*";

        Intent intent = new Intent(Intent.ACTION_VIEW);
        Uri data = Uri.fromFile(nativeFile);

        intent.setDataAndType(data, type);

        try {
            startActivity(intent);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(this.getActivity(), "Could not find application that can open " + file.getName(), Toast.LENGTH_LONG).show();
        }
    }

    private void newFolder() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this.getActivity());
        builder.setTitle("New Folder");

        // Set up the input
        final EditText input = new EditText(this.getActivity());
        // Specify the type of input expected; this, for example, sets the input
        // as a password, and will mask the text
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        builder.setView(input);

        // Set up the buttons
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                new NewDirectoryTask().execute(input.getText().toString());
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();

    }

    private void renameFile(final MyFile file) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this.getActivity());
        builder.setTitle("Rename " + file.getName() + "?");

        // Set up the input
        final EditText input = new EditText(this.getActivity());
        // Specify the type of input expected; this, for example, sets the input
        // as a password, and will mask the text
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        input.setText(file.getName());
        builder.setView(input);

        // Set up the buttons
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                new RenameFileTask(file).execute(input.getText().toString());
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();

    }

    private void updateStatus(String filePath) {
        final FragmentActivity activity = getActivity();
        if (activity != null) {
            final TextView textView = (TextView) activity.findViewById(R.id.status);
            final MyFile[] files = mFileSystem.getCurrentDirectory().getFiles();
            long bytes = 0;
            for (MyFile file : files) {
                if (file.isFile()) {
                    bytes += file.length();
                }
            }
            final String text = String.format(Locale.US, "%s - [Items: %d; Size: %s Mb]", filePath, files.length, mDecimalFormat.format((double) bytes / 1024 / 1024));
            Log.i("STATUS", text);
            textView.setText(text);
        }
    }

    public void updateStatus() {
        if (mFileSystem != null && mFileSystem.getCurrentDirectory() != null) {
            updateStatus(mFileSystem.getCurrentDirectory().getCurrentPath());
        }
    }

    private void changeState(Queue<Memento> previousStates, Queue<Memento> saveStates) {
        if (!previousStates.isEmpty()) {
            final Memento oldMemento = previousStates.poll();
            final Memento newMemento = mFileSystem.save();
            if (!saveStates.offer(newMemento)) {
                saveStates.poll();
                saveStates.offer(newMemento);
            }
            mFileSystem.restore(oldMemento);
            updateStatus();
            ((BaseAdapter) mGridView.getAdapter()).notifyDataSetInvalidated();
            FileListAdapter listAdapter = new FileListAdapter(StorageFragment.this.getActivity(), mFileSystem);
            mGridView.setAdapter(listAdapter);
            mGridView.invalidateViews();
        }
    }

    private class ChangeDirectoryTask extends AsyncTask<MyFile, Void, Void> {

        private Activity mActivity;
        private AbsListView mListView;
        private FileSystem mFileSystem;
        private ProgressDialog mProgressDialog;

        public ChangeDirectoryTask(Activity activity, AbsListView listView, FileSystem fileSystem) {
            mActivity = activity;
            mListView = listView;
            mFileSystem = fileSystem;
        }

        @Override
        protected void onPreExecute() {
            mProgressDialog = ProgressDialog.show(mActivity, "Wait", "Changing folder...");
        }

        @Override
        protected Void doInBackground(MyFile... files) {
            if (mActivity instanceof FragmentActivity) {
                ((FragmentActivity) mActivity).getSupportFragmentManager().saveFragmentInstanceState(StorageFragment.this);
            }
            if (!mUndoQueue.offer(mFileSystem.save())) {
                mUndoQueue.poll();
                mUndoQueue.offer(mFileSystem.save());
            }

            try {
                mFileSystem.changeDirectory(files[0]);
            } catch (IOException e) {
                sLogger.log(Level.WARNING, "Could not change folder to " + files[0]);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void v) {
            mProgressDialog.dismiss();
            // now that we have all the keys, add them all to the adapter
            ((BaseAdapter) mListView.getAdapter()).notifyDataSetInvalidated();
            FileListAdapter listAdapter = new FileListAdapter(StorageFragment.this.getActivity(), mFileSystem);
            mListView.setAdapter(listAdapter);
            mListView.invalidateViews();
            updateStatus(mFileSystem.getCurrentDirectory().getCurrentPath());
        }
    }

    private class RefreshTask extends AsyncTask<Void, Void, Void> {

        private ProgressDialog mProgressDialog;

        @Override
        protected void onPreExecute() {
            mProgressDialog = ProgressDialog.show(StorageFragment.this.getActivity(), "Wait", "Refreshing...");
        }

        @Override
        protected Void doInBackground(Void... v) {
            StorageFragment.this.getActivity().getSupportFragmentManager().saveFragmentInstanceState(StorageFragment.this);

            try {
                mFileSystem.getCurrentDirectory().refresh();
            } catch (IOException e) {
                sLogger.log(Level.WARNING, "Could not refresh folder " + mFileSystem.getCurrentDirectory());
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void v) {
            mProgressDialog.dismiss();
            // now that we have all the keys, add them all to the adapter
            ((BaseAdapter) mGridView.getAdapter()).notifyDataSetInvalidated();
            FileListAdapter listAdapter = new FileListAdapter(StorageFragment.this.getActivity(), mFileSystem);
            mGridView.setAdapter(listAdapter);
            mGridView.invalidateViews();
            updateStatus(mFileSystem.getCurrentDirectory().getCurrentPath());
        }
    }

    private class NewDirectoryTask extends AsyncTask<String, Void, Boolean> {

        private String mFolder;

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected Boolean doInBackground(String... folder) {
            mFolder = folder[0];
            StorageFragment.this.getActivity().getSupportFragmentManager().saveFragmentInstanceState(StorageFragment.this);
            return mFileSystem.getCurrentDirectory().newDirectory(mFolder);
        }

        @Override
        protected void onPostExecute(Boolean result) {
            refresh();
            if (!result.booleanValue()) {
                Toast.makeText(StorageFragment.this.getActivity(), String.format("Could not create %s. Please, check your permissions.", mFolder), Toast.LENGTH_LONG).show();
            }
        }
    }

    private class RenameFileTask extends AsyncTask<String, Void, Boolean> {

        private final MyFile mFile;

        public RenameFileTask(MyFile file) {
            mFile = file;
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected Boolean doInBackground(String... newName) {
            StorageFragment.this.getActivity().getSupportFragmentManager().saveFragmentInstanceState(StorageFragment.this);
            return mFileSystem.getCurrentDirectory().rename(mFile, newName[0]);
        }

        @Override
        protected void onPostExecute(Boolean result) {
            refresh();
            if (!result.booleanValue()) {
                Toast.makeText(StorageFragment.this.getActivity(), String.format("Could not rename %s. Please, check your permissions.", mFile.getName()), Toast.LENGTH_LONG)
                        .show();
            }
        }
    }

    private class DeleteTask extends AsyncTask<MyFile, Void, Boolean> {

        private ProgressDialog mProgressDialog;

        private MyFile mFile = null;

        @Override
        protected void onPreExecute() {
            mProgressDialog = ProgressDialog.show(StorageFragment.this.getActivity(), "Wait", "Deleting...");
        }

        @Override
        protected Boolean doInBackground(MyFile... v) {

            StorageFragment.this.getActivity().getSupportFragmentManager().saveFragmentInstanceState(StorageFragment.this);
            mFile = v[0];
            boolean result = false;
            try {
                result = mFileSystem.getCurrentDirectory().delete(mFile);
                mFileSystem.getCurrentDirectory().refresh();
            } catch (IOException e) {
                sLogger.log(Level.WARNING, "Could not refresh folder " + mFileSystem.getCurrentDirectory());
            }
            return result;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            mProgressDialog.dismiss();
            if (result && mFile != null) {
                Toast.makeText(StorageFragment.this.getActivity(), String.format("File %s successfully deleted.", mFile.getName()), Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(StorageFragment.this.getActivity(), String.format("Could not delete %s. Check your permissions.", mFile.getName()), Toast.LENGTH_LONG).show();
            }
            // now that we have all the keys, add them all to the adapter
            ((BaseAdapter) mGridView.getAdapter()).notifyDataSetInvalidated();
            FileListAdapter listAdapter = new FileListAdapter(StorageFragment.this.getActivity(), mFileSystem);
            mGridView.setAdapter(listAdapter);
            mGridView.invalidateViews();
            updateStatus(mFileSystem.getCurrentDirectory().getCurrentPath());
        }
    }

    private class InitializeTask extends AsyncTask<Void, Void, Void> {

        private Activity mActivity;
        private AbsListView mListView;
        private FileSystem mFileSystem;
        private ProgressDialog mProgressDialog;

        public InitializeTask(Activity activity, AbsListView listView, FileSystem fileSystem) {
            mActivity = activity;
            mListView = listView;
            mFileSystem = fileSystem;
        }

        @Override
        protected void onPreExecute() {
            mProgressDialog = ProgressDialog.show(mActivity, "Wait", "Fetching information...");

        }

        @Override
        protected Void doInBackground(Void... v) {
            if (mActivity instanceof FragmentActivity) {
                ((FragmentActivity) mActivity).getSupportFragmentManager().saveFragmentInstanceState(StorageFragment.this);
            }

            try {
                mFileSystem.init();
            } catch (IOException e) {
                sLogger.log(Level.WARNING, "Could not refresh folder " + mFileSystem.getCurrentDirectory());
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void v) {
            mProgressDialog.dismiss();
            final BaseAdapter adapter = (BaseAdapter) mGridView.getAdapter();
            if (adapter != null) {
                adapter.notifyDataSetInvalidated();
            }
            FileListAdapter listAdapter = new FileListAdapter(StorageFragment.this.getActivity(), mFileSystem);
            mListView.setAdapter(listAdapter);
            mListView.invalidateViews();
            updateStatus(mFileSystem.getCurrentDirectory().getCurrentPath());
        }
    }

}
