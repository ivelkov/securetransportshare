package ivelkov.fileshare.presentation;

import android.widget.ImageView;
import android.widget.TextView;

public class ViewHolder {
    
    private final ImageView mViewImage;
    private final TextView mTextView;
    private boolean mSelected = false;
    
    /**
     * @param viewImage
     * @param textView
     */
    public ViewHolder(ImageView viewImage, TextView textView) {
        mViewImage = viewImage;
        mTextView = textView;
    }
    
    public ImageView getViewImage() {
        return mViewImage;
    }

    public TextView getTextView() {
        return mTextView;
    }

    public boolean isSelected() {
        return mSelected;
    }

    public void setSelected(boolean selected) {
        mSelected = selected;
    }

}
