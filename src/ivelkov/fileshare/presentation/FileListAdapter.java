package ivelkov.fileshare.presentation;

import ivelkov.fileshare.R;
import ivelkov.fileshare.model.FileSystem;
import ivelkov.fileshare.model.MyFile;
import android.app.Activity;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class FileListAdapter extends BaseAdapter {

    private final FileSystem mFileSystem;
    private final Activity mActivity;

    public FileListAdapter(Activity activity, FileSystem fileSystem) {
        mActivity = activity;
        mFileSystem = fileSystem;
    }

    @Override
    public int getCount() {
        Log.v(FileListAdapter.class.getName(),
                String.format("Folder %s; Files %d", mFileSystem.getCurrentDirectory().getCurrentPathFile().getName(), mFileSystem.getCurrentDirectory().getCount()));
        return mFileSystem.getCurrentDirectory().getCount();
    }

    @Override
    public Object getItem(int position) {
        final String name = mFileSystem.getCurrentDirectory().getFile(position).getName();
        final String folder = mFileSystem.getCurrentDirectory().getCurrentPathFile().getName();
        Log.v(FileListAdapter.class.getName(), String.format("getItem - Position %d [Folder %s; File %s; Item %d]", position, folder, name, position));
        return name;
    }

    @Override
    public long getItemId(int position) {
        final Object item = getItem(position);
        final int id = item.hashCode();
        Log.v(FileListAdapter.class.getName(), String.format("getItemId - Position %d [Name %s; Item %d; Id: %d]", position, item, position, id));
        return id;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder view;
        LayoutInflater inflator = mActivity.getLayoutInflater();
        Log.v(FileListAdapter.class.getName(), String.format("getView(%d, ..., ...)", position));
        final MyFile file = mFileSystem.getCurrentDirectory().getFile(position);
        final String folder = mFileSystem.getCurrentDirectory().getCurrentPathFile().getName();
        Log.v(FileListAdapter.class.getName(), String.format("getView - Position %d [Folder %s; File %s; Item %d]", position, folder, file.getName(), position));

        if (convertView == null) {
            convertView = inflator.inflate(R.layout.gridview_item, null);
        }
        final TextView textView = (TextView) convertView.findViewById(R.id.textView1);
        textView.setText(file.getName());
        AbsListView gridView = (AbsListView) parent.findViewWithTag("files");
        final SparseBooleanArray checkedItems = gridView.getCheckedItemPositions();
        final boolean isChecked = checkedItems != null ? checkedItems.get(position) : false;
        final ImageView imageView = (ImageView) convertView.findViewById(R.id.imageView1);
        if (isChecked) {
            imageView.setImageResource(file.isDirectory() ? R.drawable.folder_accept : R.drawable.file_accept);
        } else {
            imageView.setImageResource(file.isDirectory() ? R.drawable.folder_full : R.drawable.file);
        }
        view = new ViewHolder(imageView, textView);
        convertView.setTag(view);
        return convertView;
    }

}
