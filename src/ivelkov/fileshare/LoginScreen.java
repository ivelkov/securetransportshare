package ivelkov.fileshare;

import java.io.IOException;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender.SendIntentException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;

import com.google.android.gms.auth.GoogleAuthException;
import com.google.android.gms.auth.GoogleAuthUtil;
import com.google.android.gms.auth.UserRecoverableAuthException;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.plus.Plus;

public class LoginScreen extends Activity implements OnConnectionFailedListener, ConnectionCallbacks, OnClickListener {

    private enum LoginType {
        GOOGLE

    };

    private LoginType loginRequestType;

    private static final String LOG_TAG = "LoginActivity";

    // Google SignIn Variables
    public static final int RC_SIGN_IN = 0;
    public static final int RC_SIGN_OUT = 1;

    private GoogleApiClient mGoogleAPIClient;
    private ConnectionResult connectionResult;
    private boolean intentInProgress;
    private boolean signInClicked;
    private boolean wipeOutClicked;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_screen);

        Log.d(LOG_TAG, "onCreate");

        CognitoSyncClientManager.init(this);
        initGoogleApliClient();
        loginRequestType = LoginType.GOOGLE;
        final SignInButton gmailButton = (SignInButton) findViewById(R.id.btnLoginGmail);
        gmailButton.setSize(SignInButton.SIZE_WIDE);
        gmailButton.setOnClickListener(this);

    }

    private void initGoogleApliClient() {

        mGoogleAPIClient = new GoogleApiClient.Builder(this).addConnectionCallbacks((GoogleApiClient.ConnectionCallbacks) this)
                .addOnConnectionFailedListener((GoogleApiClient.OnConnectionFailedListener) this).addApi(Plus.API).addScope(Plus.SCOPE_PLUS_PROFILE).build();

    }

    private void startAppActivity(boolean withIdentity) {
        Intent intent = new Intent(LoginScreen.this, MainActivity.class);
        if (withIdentity)
            intent.putExtra("withIdentity", withIdentity);
        startActivityForResult(intent, 2);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN) {

            if (resultCode != RESULT_OK) {
                signInClicked = false;
            }

            intentInProgress = false;

            if (!mGoogleAPIClient.isConnecting()) {
                mGoogleAPIClient.connect();
            }

        } else if (requestCode == 2 && resultCode == RC_SIGN_OUT) {
            wipeOutClicked = true;
            new AlertDialog.Builder(LoginScreen.this).setTitle("Wipe all data?").setMessage("This will log off your current session and wipe all user data.\r\nAll data will be lost.")
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            wipeOutClicked = false;
                            // clear login status
                            wipeout();
                        }

                    }).setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            wipeOutClicked = false;
                            dialog.cancel();
                        }
                    }).show();
        }
    }

    /**
     * A task to refresh credentials after adding facebook logins
     */
    /*
     * Added Google login to RefreshCredentials Overloaded constructor
     */
    class RefreshCredentialsTask extends AsyncTask<Void, Void, Void> {

        // ProgressDialog dialog;
        LoginType identityType;
        Context context = null;

        public RefreshCredentialsTask(LoginType identityType, Context context) {
            this.identityType = identityType;
            this.context = context;
        }

        public RefreshCredentialsTask(LoginType identityType) {
            this.identityType = identityType;
        }

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected Void doInBackground(Void... params) {

            Log.i(LOG_TAG, "google clientid: " + getString(R.string.google_client_id));
            if (mGoogleAPIClient.isConnected()) {
                String accessToken = null;
                final String accountName = Plus.AccountApi.getAccountName(mGoogleAPIClient);
                try {
                    accessToken = GoogleAuthUtil.getToken(context, accountName, "audience:server:client_id:" + getString(R.string.google_service_client_id));

                    CognitoSyncClientManager.addLogins("accounts.google.com", accessToken);
                    Log.i(LOG_TAG, "Successfully registered account name: " + accountName + "; google token: " + accessToken);
                    startAppActivity(false);
                } catch (UserRecoverableAuthException e) {
                    Log.e(LOG_TAG, "Could not register " + accountName);
                } catch (IOException e) {
                    Log.e(LOG_TAG, "Could not register " + accountName);
                } catch (GoogleAuthException e) {
                    Log.e(LOG_TAG, "Could not register " + accountName);
                }
            }

            return null;
        }

    }

    @Override
    protected void onStop() {
        super.onStop();

        if (mGoogleAPIClient.isConnected()) {
            mGoogleAPIClient.disconnect();
        }

    }

    protected void onStart() {
        super.onStart();

        if (!mGoogleAPIClient.isConnected()) {
            mGoogleAPIClient.connect();
        }

    }

    @Override
    public void onConnected(Bundle arg0) {
        if (!wipeOutClicked) {
            signInClicked = false;

            // Retrieve the oAuth 2.0 access token.
            final Context context = this.getApplicationContext();

            new RefreshCredentialsTask(loginRequestType, context).execute();
        }

    }

    @Override
    public void onConnectionFailed(ConnectionResult connResult) {
        Log.i(LOG_TAG, "onConnectionFailed: ConnectionResult.getErrorCode() = " + connResult.getErrorCode());
        if (!intentInProgress) {
            connectionResult = connResult;

            if (signInClicked) {
                resolveSignInError();
            }
        }
    }

    @Override
    public void onConnectionSuspended(int cause) {
        mGoogleAPIClient.connect();

    }

    /* A helper method to resolve the current ConnectionResult error. */
    private void resolveSignInError() {
        if (connectionResult != null) {
            if (connectionResult.hasResolution()) {
                try {
                    intentInProgress = true;
                    startIntentSenderForResult(connectionResult.getResolution().getIntentSender(), RC_SIGN_IN, null, 0, 0, 0);
                } catch (SendIntentException e) {
                    Log.i(LOG_TAG, "Sign in intent could not be sent: " + e.getLocalizedMessage());
                    // The intent was canceled before it was sent. Return to the
                    // default
                    // state and attempt to connect to get an updated
                    // ConnectionResult.
                    intentInProgress = false;
                    mGoogleAPIClient.connect();
                }
            }
        } else {
            mGoogleAPIClient.connect();
        }
    }

    @Override
    public void onClick(View v) {
        wipeOutClicked = false;
        if (v.getId() == R.id.btnLoginGmail && !mGoogleAPIClient.isConnecting()) {
            signInClicked = true;
            resolveSignInError();
        }

    }

    private void wipeout() {
        new WipeoutTask(this).execute(mGoogleAPIClient);
        initGoogleApliClient();
    }

}
