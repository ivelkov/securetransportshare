/**
 * Copyright 2010-2014 Amazon.com, Inc. or its affiliates. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License.
 * A copy of the License is located at
 *
 *  http://aws.amazon.com/apache2.0
 *
 * or in the "license" file accompanying this file. This file is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

package ivelkov.fileshare;

import java.util.HashMap;
import java.util.Map;

import android.content.Context;

import com.amazonaws.SDKGlobalConfiguration;
import com.amazonaws.auth.CognitoCachingCredentialsProvider;
import com.amazonaws.mobileconnectors.cognito.CognitoSyncManager;
import com.amazonaws.regions.Regions;

public class CognitoSyncClientManager {

    private static CognitoSyncManager client;
    private static CognitoCachingCredentialsProvider provider;
    private static String sAccountName;

    private static String sBucketName;
    
    
    static {
        System.setProperty(SDKGlobalConfiguration.ENABLE_S3_SIGV4_SYSTEM_PROPERTY, "true");
    }

    /**
     * Initializes the CognitoClient. This must be called before getInstance().
     * 
     * @param context a context of the app
     */
    public static void init(Context context) {
        final String awsAccountId = context.getString(R.string.aws_account_id);
        final String identityPoolId = context.getString(R.string.aws_identity_pool);
        final String authRoleArn = context.getString(R.string.aws_arn_role);
        final String unauthRoleArn = context.getString(R.string.aws_anon_arn_role);
        sBucketName = context.getString(R.string.bucket_name);
        
        provider = new CognitoCachingCredentialsProvider(context,
                awsAccountId, identityPoolId, unauthRoleArn, authRoleArn,Regions.EU_WEST_1);

        client = new CognitoSyncManager(context, identityPoolId, Regions.EU_WEST_1, provider);
    }

    /**
     * Sets the login so that you can use authorized identity. This requires a
     * network request. Please call it in a background thread.
     * 
     * @param providerName the name of 3rd identity provider
     * @param token openId token
     */
    public static void addLogins(String providerName, String token) {
        if (client == null) {
            throw new IllegalStateException("client not initialized yet");
        }
 
        Map<String, String> logins = provider.getLogins();
        if (logins == null) {
            logins = new HashMap<String, String>();
        }
        logins.put(providerName, token);
        provider.setLogins(logins);
        sAccountName = provider.getIdentityId();
    }

    /**
     * Gets the singleton instance of the CognitoClient. init() must be call
     * prior to this.
     * 
     * @return an instance of CognitoClient
     */
    public static CognitoSyncManager getInstance() {
        if (client == null) {
            throw new IllegalStateException("client not initialized yet");
        }
        return client;
    }
    
    public static CognitoCachingCredentialsProvider getCachingCredentialsProvider() {
        return provider;
    }

//
//    public static void setAccountName(String accountName) {
//        sAccountName = accountName;
//    }
    
    public static String getAccountName() {
        return sAccountName;
    }
    
    public static String getBucketName() {
        return sBucketName;
    }

    public static void setBucketName(String bucketName) {
        sBucketName = bucketName;
    }
}
