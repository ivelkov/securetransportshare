package ivelkov.fileshare.network;

import ivelkov.fileshare.model.MyFile;
import ivelkov.fileshare.model.TransferModel;
import android.content.Context;
import android.content.Intent;

/*
 * This class is a bridge to the NetworkService, making it easy to have the service
 * do operations for us. This just makes it so that we don't have to worry about
 * missing parameters in the Intent and that kind of thing
 */
public class TransferController {
    public static void abort(Context context, TransferModel model) {
        Intent intent = makeIdIntent(context, model.getId());
        intent.setAction(NetworkService.ACTION_ABORT);
        context.startService(intent);
    }

    public static void upload(Context context, MyFile localFile, MyFile remoteDirectory) {
        Intent intent = new Intent(context, NetworkService.class);
        intent.setAction(Intent.ACTION_SEND);
        intent.putExtra(NetworkService.UPLOAD_LOCAL_FILE_KEY, localFile);
        intent.putExtra(NetworkService.UPLOAD_REMOTE_DIR_KEY, remoteDirectory);
        context.startService(intent);
    }

    public static void download(Context context, String[] keys) {
        Intent intent = new Intent(context, NetworkService.class);
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.putExtra(NetworkService.S3_KEYS_EXTRA, keys);
        context.startService(intent);
    }

    public static void pause(Context context, TransferModel model) {
        Intent intent = makeIdIntent(context, model.getId());
        intent.setAction(NetworkService.ACTION_PAUSE);
        context.startService(intent);
    }

    public static void resume(Context context, TransferModel model) {
        Intent intent = makeIdIntent(context, model.getId());
        intent.setAction(NetworkService.ACTION_RESUME);
        context.startService(intent);
    }

    private static Intent makeIdIntent(Context context, int id) {
        Intent intent = new Intent(context, NetworkService.class);
        intent.putExtra(NetworkService.NOTIF_ID_EXTRA, id);
        return intent;
    }
}
