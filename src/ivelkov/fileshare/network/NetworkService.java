package ivelkov.fileshare.network;

import ivelkov.fileshare.CognitoSyncClientManager;
import ivelkov.fileshare.model.DownloadModel;
import ivelkov.fileshare.model.MyFile;
import ivelkov.fileshare.model.TransferModel;
import ivelkov.fileshare.model.UploadModel;
import android.app.IntentService;
import android.content.Intent;

import com.amazonaws.mobileconnectors.s3.transfermanager.TransferManager;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;

/*
 * This class handles starting all the downloads/uploads. We use a service to do this
 * so that the transfers will continue even though the activity has ended(ie due to
 * orientation change).
 *
 */
public class NetworkService extends IntentService {

    
    public static final String S3_KEYS_EXTRA = "keys";
    public static final String ACTION_ABORT = "abort";
    public static final String ACTION_PAUSE = "pause";
    public static final String ACTION_RESUME = "resume";
    public static final String NOTIF_ID_EXTRA = "notification_id";

    public static final String UPLOAD_LOCAL_FILE_KEY = "localFile";
    public static final String UPLOAD_REMOTE_DIR_KEY = "remoteFile";

    private static final String TAG = "NetworkService";
    private static final int DEFAULT_INT = -1;

    private TransferManager mTransferManager;

    public NetworkService() {
        super(TAG);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        final AmazonS3 s3Client = new AmazonS3Client(CognitoSyncClientManager.getCachingCredentialsProvider());
        s3Client.setRegion(Region.getRegion(Regions.EU_CENTRAL_1));
        mTransferManager = new TransferManager(s3Client);
    }

    @Override
    public void onHandleIntent(Intent intent) {
        if (intent != null && intent.getAction() != null) {
            if (intent.getAction().equals(Intent.ACTION_GET_CONTENT) && intent.getSerializableExtra(UPLOAD_LOCAL_FILE_KEY) != null
                    && intent.getSerializableExtra(UPLOAD_REMOTE_DIR_KEY) != null) {
                final MyFile localFile = (MyFile) intent.getSerializableExtra(UPLOAD_LOCAL_FILE_KEY);
                final MyFile remoteFile = (MyFile) intent.getSerializableExtra(UPLOAD_REMOTE_DIR_KEY);
                download(localFile, remoteFile);
            } else if (intent.getAction().equals(Intent.ACTION_SEND) && intent.getSerializableExtra(UPLOAD_LOCAL_FILE_KEY) != null
                    && intent.getSerializableExtra(UPLOAD_REMOTE_DIR_KEY) != null) {
                final MyFile localFile = (MyFile) intent.getSerializableExtra(UPLOAD_LOCAL_FILE_KEY);
                final MyFile remoteFile = (MyFile) intent.getSerializableExtra(UPLOAD_REMOTE_DIR_KEY);
                upload(localFile, remoteFile);
            } else if (intent.getIntExtra(NOTIF_ID_EXTRA, DEFAULT_INT) != DEFAULT_INT) {
                int notifId = intent.getIntExtra(NOTIF_ID_EXTRA, DEFAULT_INT);
                if (intent.getAction().equals(ACTION_PAUSE)) {
                    pause(notifId);
                } else if (intent.getAction().equals(ACTION_ABORT)) {
                    abort(notifId);
                } else if (intent.getAction().equals(ACTION_RESUME)) {
                    resume(notifId);
                }
            }
        }
    }

    private void abort(int notifId) {
        TransferModel model = TransferModel.getTransferModel(notifId);
        model.abort();
    }

    private void download(MyFile localFile, MyFile remoteFile) {
        DownloadModel model = new DownloadModel(this, localFile, remoteFile, mTransferManager);
        model.download();
    }

    private void pause(int notifId) {
        TransferModel model = TransferModel.getTransferModel(notifId);
        model.pause();
    }

    private void resume(int notifId) {
        TransferModel model = TransferModel.getTransferModel(notifId);
        model.resume();
    }

    /* We use a new thread for upload because we have to copy the file */
    private void upload(MyFile localFile, MyFile remoteFile) {
        final UploadModel model = new UploadModel(this, localFile, remoteFile, mTransferManager);
        new Thread(model.getUploadRunnable()).run();
    }
}
